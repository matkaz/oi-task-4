#!/bin/bash

if [ ! -e 'bin/solution1' ]; then
    echo 'Binaries were not build correctly. Rebuilding...'
    ./build.sh
fi

# Train
bin/Task4 -t ../../environment/data/train

# Deploy
if [ ! -d 'deploy' ]; then
    mkdir deploy
fi
cp bin/Task4 deploy/Task4
