//
// Created by mateusz on 03.06.17.
//

#ifndef TASK4_TRAINER_H
#define TASK4_TRAINER_H

#include <string>
#include <experimental/filesystem>
#include "BoundingBox.h"

class Trainer {

public:
    Trainer(const std::string &trainingDataDirName_);

    void train();

    const static double negativesRatio;

private:
    const std::string imagesDir;
    const std::string annotationsDir;

    void loadAnnotations(const std::experimental::filesystem::directory_entry &it, std::vector<BoundingBox> &bbs) const;

    void addNegativeExamples(std::vector<BoundingBox> &bbs, const cv::Mat &image) const;

    void extractDescriptors(const std::vector<BoundingBox> &bbs, const cv::Mat &image,
                            std::vector<std::vector<float>> &outputDescriptors) const;

    void loadDescriptors(std::vector<std::pair<int, std::vector<float>>> &trainingData,
                         const std::experimental::filesystem::directory_entry &it) const;

    void loadTrainingDataWithLabels(cv::Mat &trainingMats, cv::Mat &labels) const;
};


#endif //TASK4_TRAINER_H
