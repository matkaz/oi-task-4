//
// Created by mateusz on 29.04.17.
//

#include <algorithm>
#include <sstream>
#include "BoundingBox.h"

BoundingBox::BoundingBox() : right(0), down(0), left(0), up(0) {

}

BoundingBox::BoundingBox(double right, double down, double left, double up) : right(right), down(down), left(left),
                                                                              up(up) {
    if (right <= left || down <= up) {
        throw BoundingBoxException("Invalid bounding box: " + (std::string)(*this));
    }
}

BoundingBox::BoundingBox(std::istream &input) {
    input >> this->left >> this->up >> this->right >> this->down;
    if (input.fail()) {
        throw BoundingBoxException("Can't read bounding box from stream!");
    }
}

BoundingBox::BoundingBox(const cv::Rect &rect) : right(rect.x + rect.width), down(rect.y + rect.height),
                                                 left(rect.x), up(rect.y) {
}

double BoundingBox::getArea() const {
    const double a = right - left;
    const double b = down - up;
    return a*b;
}

double BoundingBox::intersectionOverUnion(const BoundingBox &that) const {
    const double area = getArea();
    const double intersectionArea = getIntersectionArea(that);
    const double unionArea = area + that.getArea() - intersectionArea;
    return intersectionArea / unionArea;
}

const double BoundingBox::getIntersectionArea(const BoundingBox &that) const {
    try {
        BoundingBox hypotheticalIntersection = (*this) * that;
        return hypotheticalIntersection.getArea();
    } catch (BoundingBoxException& bbe) {
        return 0;
    }
}

BoundingBox BoundingBox::operator*(const BoundingBox &that) const {
    return BoundingBox(
            std::min(this->right, that.right),
            std::min(this->down, that.down),
            std::max(this->left, that.left),
            std::max(this->up, that.up));
}

BoundingBox::operator std::string() const {
    std::ostringstream oss;
    oss << *this;
    return oss.str();
}

BoundingBox::operator cv::Rect_<int>() const {
    return cv::Rect(left, up, right - left, down - up);
}

std::ostream &operator<<(std::ostream &os, const BoundingBox &boundingBox) {
    os << boundingBox.left << " " << boundingBox.up << " " << boundingBox.right << " " << boundingBox.down;
}

void BoundingBox::enlarge(const BoundingBox &box) {
    this->right = std::max(this->right, box.right);
    this->down = std::max(this->down, box.down);
    this->left = std::min(this->left, box.left);
    this->up = std::min(this->up, box.up);
}

double BoundingBox::getRight() const {
    return right;
}

double BoundingBox::getDown() const {
    return down;
}

double BoundingBox::getLeft() const {
    return left;
}

double BoundingBox::getUp() const {
    return up;
}

double BoundingBox::getWidth() const {
    return right - left;
}

double BoundingBox::getHeight() const {
    return down - up;
}
