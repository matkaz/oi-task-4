//
// Created by mateusz on 07.05.17.
//

#ifndef SOLUTION1_DETECTOR_H
#define SOLUTION1_DETECTOR_H


#include <vector>
#include <ostream>
#include "BoundingBox.h"
#include "classes.h"
#include <memory>

#include <opencv2/core.hpp>
#include <opencv2/objdetect.hpp>
#include <opencv/ml.h>

class Detector {
public:
    Detector(const cv::String& imageFileName);

    void testImage();
    bool testWindow(const unsigned int& upperLeftX, const unsigned int& upperLeftY, cv::Mat& workingImage);

    void addToBoxes(unsigned int x, unsigned int y, const unsigned int& s);

    void drawBoxes() const;
    void printAnnotations(std::ostream& os) const;

    typedef std::runtime_error DetectorException;

    const static unsigned int windowHeight;
    const static unsigned int windowWidth;
    const static cv::Size windowSize;
    const static unsigned int windowStepY;
    const static unsigned int windowStepX;
    const static std::string savedSVM;
    cv::Ptr<cv::ml::SVM> loadedSVM;
    const static double scaleFactor;
    const static unsigned int initialWidth;

    static void getDescriptors(const cv::Mat &currentWindow, std::vector<float> &descriptors);
private:
    cv::Mat image;
    double initialScale;
    std::vector<BoundingBox> resultBoxes;
};


#endif //SOLUTION1_DETECTOR_H
