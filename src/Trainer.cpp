//
// Created by mateusz on 03.06.17.
//

#include "Trainer.h"
#include "Detector.h"

#include <experimental/filesystem>
#include <fstream>
#include <cstdlib>
#include <ctime>
#include <algorithm>
#include <iostream>
#include <vector>
#include <utility>
#include <string>

#include <opencv2/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/ml.hpp>

using namespace std::experimental::filesystem;
using namespace cv;
using namespace ml;

const double Trainer::negativesRatio = 99. / 100.;

Trainer::Trainer(const std::string &trainingDataDirName_) :
        imagesDir(trainingDataDirName_ + "/images"),
        annotationsDir(trainingDataDirName_ + "/annotations") {
}

void Trainer::train() {
    std::srand(std::time(0));

    Mat trainingMats, labels;
    loadTrainingDataWithLabels(trainingMats, labels);

    std::clog << "Training SVM..." << std::endl;
    Ptr<SVM> svm = SVM::create();
    svm->setType(SVM::C_SVC);
    svm->setKernel(SVM::LINEAR);
    svm->setTermCriteria(TermCriteria(TermCriteria::MAX_ITER, 1000000, 1e-6));
    svm->train(trainingMats, ROW_SAMPLE, labels);

    std::clog << "Saving SVM..." << std::endl;
    svm->save(Detector::savedSVM);

    std::clog << "SVM was saved to " << Detector::savedSVM << std::endl;
}

void Trainer::loadTrainingDataWithLabels(Mat &trainingMats, Mat &labels) const {
    std::clog << "Retrieving training data..." << std::endl;
    std::vector<std::pair<int, std::vector<float>>> trainingData;
    for (directory_iterator it = begin(directory_iterator(imagesDir)); it != end(directory_iterator(imagesDir)); it++) {
        loadDescriptors(trainingData, *it);
    }

    std::clog << "Shuffling data..." << std::endl;
    random_shuffle(trainingData.begin(), trainingData.end());

    std::clog << "Copying data to desired format..." << std::endl;
    trainingMats = Mat(trainingData.size(), trainingData[0].second.size(), CV_32FC1);
    labels = Mat(trainingData.size(), 1, CV_32SC1);
    for (int i = 0; i < trainingMats.rows; ++i) {
        labels.at<int>(i, 0) = trainingData[i].first;
        for (int j = 0; j < trainingMats.cols; ++j) {
            trainingMats.at<float>(i, j) = trainingData[i].second[j];
        }
    }
}

void Trainer::loadDescriptors(std::vector<std::pair<int, std::vector<float>>> &trainingData, const directory_entry &it) const {
    std::vector<BoundingBox> bbs;
    loadAnnotations(it, bbs);
    Mat image = imread(it.path().string());
    const size_t positives = bbs.size();
    addNegativeExamples(bbs, image);
    std::vector<std::vector<float>> descriptors(bbs.size());
    extractDescriptors(bbs, image, descriptors);

    for (size_t i = 0; i < positives; ++i) {
        trainingData.push_back(make_pair(ROAD_SIGN, descriptors[i]));
    }
    for (size_t i = positives; i < descriptors.size(); ++i) {
        trainingData.push_back(make_pair(BACKGROUND, descriptors[i]));
    }
}

void Trainer::extractDescriptors(const std::vector<BoundingBox> &bbs, const Mat &image,
                                 std::vector<std::vector<float>> &outputDescriptors) const {
    for (int i = 0; i < bbs.size(); ++i) {
            Mat signExtracted = image((Rect)bbs[i]);
            Mat signResized = Mat(Detector::windowSize, signExtracted.type());
            resize(signExtracted, signResized, Detector::windowSize);

            std::vector<float> descriptors;
            Detector::getDescriptors(signResized, descriptors);
            outputDescriptors[i] = descriptors;
        }
}

void Trainer::addNegativeExamples(std::vector<BoundingBox> &bbs, const Mat &image) const {
    const size_t positives = bbs.size();
    const size_t negatives = (size_t) std::round((negativesRatio * (double)positives) / (1. - negativesRatio));
    bbs.resize(positives + negatives);
    for (int i = 0; i < negatives; ++i) {
            bool ok = false;

            while (!ok) {
                const unsigned int upperLeftX = rand() % (unsigned int)(image.cols - bbs[i].getWidth());
                const unsigned int upperLeftY = rand() % (unsigned int)(image.rows - bbs[i].getHeight());
                const unsigned int size = (unsigned int)(std::pow((1./Detector::scaleFactor), 1 + rand() % 11) * Detector::windowWidth);

                BoundingBox randomBox(upperLeftX + size,
                                      upperLeftY + size,
                                      upperLeftX,
                                      upperLeftY);

                ok = true;

                if (upperLeftX + size > image.cols || upperLeftY + size > image.rows) {
                    ok = false;
                    continue;
                }

                for (int j = 0; j < positives; ++j) {
                    if (randomBox.intersectionOverUnion(bbs[j]) > 0.5) {
                        ok = false;
                        break;
                    }
                }

                if (ok) {
                    bbs[i + positives] = randomBox;
                }
            }
        }
}

void Trainer::loadAnnotations(const directory_entry &it, std::vector<BoundingBox> &bbs) const {
    const std::string stem = (it.path()).stem();
    const std::string annotationsFile = this->annotationsDir + "/" + stem + ".dat";
    std::ifstream annotationsStream(annotationsFile);
    while (!annotationsStream.eof()) {
            try {
                BoundingBox bb = BoundingBox(annotationsStream);
                bbs.push_back(bb);
            } catch (BoundingBox::BoundingBoxException& ex) {
                continue;
            }
        }
}
