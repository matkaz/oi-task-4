//
// Created by mateusz on 07.05.17.
//

#include "Detector.h"
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/ml.hpp>
#include <opencv2/imgproc.hpp>

#include <cmath>

using namespace cv;
using namespace cv::ml;

const unsigned int Detector::windowHeight = 32;
const unsigned int Detector::windowWidth = 32;
const cv::Size Detector::windowSize = cv::Size(windowWidth, windowHeight);
const unsigned int Detector::windowStepY = 8;
const unsigned int Detector::windowStepX = 8;
const std::string Detector::savedSVM = "data/SVM.svm";
const double Detector::scaleFactor = 0.8;
const unsigned int Detector::initialWidth = 1280;

Detector::Detector(const String &imageFileName) :
    image(imread(imageFileName, IMREAD_COLOR)) {
    if (image.empty()) {
        throw DetectorException("Error while reading image!");
    }

    initialScale = (double)initialWidth / (double)image.cols;
    if (initialScale != 1.0) {
        Mat imageResized;
        resize(image, imageResized, Size(initialWidth, std::round((double) image.rows * initialScale)));
        image = imageResized;
    }
}

void Detector::testImage() {
    Mat workingImage = image;
    bool windowFitsInWorkingImage = ((windowWidth * 5) <= workingImage.cols) && ((windowHeight * 3) <= workingImage.rows);
    for (int s = 0; windowFitsInWorkingImage; ++s) {
        std::clog << "Current working image: " << workingImage.size() << std::endl;
        for (unsigned int y = 0; y < workingImage.rows - windowHeight; y += windowStepY) {
            for (unsigned int x = 0; x < workingImage.cols - windowWidth; x += windowStepX) {
                // TODO skip existing boxes.
                if (testWindow(x, y, workingImage)) {
                    addToBoxes(x, y, s);
                }
            }
            std::clog << "\r" << 100.*((double)y / (double)(workingImage.rows - windowHeight)) << "%";
        }

        std::clog << std::endl;

        Mat newWorkingImage = Mat(windowSize, workingImage.type());
        resize(workingImage, newWorkingImage, Size(std::round((double)workingImage.cols * scaleFactor), std::round((double)workingImage.rows * scaleFactor)));
        workingImage = newWorkingImage;
        windowFitsInWorkingImage = ((windowWidth * 5) <= workingImage.cols) && ((windowHeight * 3) <= workingImage.rows);
    }
}

bool Detector::testWindow(const unsigned int &upperLeftX, const unsigned int &upperLeftY, cv::Mat& workingImage) {
    Mat currentWindow = workingImage(Rect_<unsigned int>(upperLeftX, upperLeftY, windowWidth, windowHeight));
    std::vector<float> descriptors;
    getDescriptors(currentWindow, descriptors);

    Mat descriptorsMat = Mat(1, descriptors.size(), CV_32FC1);
    for (int i = 0; i < descriptorsMat.cols; ++i) {
        descriptorsMat.at<float>(0, i) = descriptors[i];
    }

    if (!loadedSVM) {
        loadedSVM = SVM::load(savedSVM);
    }
    DataClass resultClass = (DataClass)((int) loadedSVM->predict(descriptorsMat));
    return resultClass == DataClass::ROAD_SIGN;
}

void Detector::getDescriptors(const Mat &currentWindow, std::vector<float> &descriptors) {
    descriptors.clear();

    Mat hsvWindow;
    cvtColor(currentWindow, hsvWindow, COLOR_BGR2HSV);
    // 32x32 mean of HSV color (whole window)
    const unsigned int slices = 32;
    const unsigned int subWindowHeight = currentWindow.rows / slices;
    const unsigned int subWindowWidth = currentWindow.cols / slices;
    for (int i = 0; i < slices; ++i) {
        for (int j = 0; j < slices; ++j) {
            Mat subWindow = hsvWindow(Rect_<unsigned int>(j*subWindowWidth, i*subWindowHeight, subWindowWidth, subWindowHeight));

            Scalar meanColor = cv::mean(subWindow); // It's "3d scalar". See OpenCV manual
            descriptors.push_back(meanColor[0]);
            descriptors.push_back(meanColor[1]);
            descriptors.push_back(meanColor[2]);
        }
    }
}

void Detector::addToBoxes(unsigned int x, unsigned int y, const unsigned int& s) {
    double scale = std::pow(scaleFactor, s);
    x = (unsigned int) std::round((double)x / scale);
    y = (unsigned int) std::round((double)y / scale);
    unsigned int scaledWinWidth = (unsigned int) std::round((double)windowWidth / scale);
    unsigned int scaledWinHeight = (unsigned int) std::round((double)windowHeight / scale);
    
    BoundingBox thisBox(x + scaledWinWidth, y + scaledWinHeight, x, y);
//    for (int i = 0; i < resultBoxes.size(); ++i) {
//        if (thisBox.getIntersectionArea(resultBoxes[i])) {
//            resultBoxes[i].enlarge(thisBox);
//            return;
//        }
//    }
    // None box was enlarged
    resultBoxes.push_back(thisBox);
}

void Detector::printAnnotations(std::ostream &os) const {
    if (resultBoxes.empty()) {
        return;
    }
    for (int i = 0; i < resultBoxes.size(); ++i) {
        os << resultBoxes[i] << std::endl;
    }
}

void Detector::drawBoxes() const {
    namedWindow("Result");
    Mat boxedImage = image.clone();
    for (const BoundingBox& bb : resultBoxes) {
        rectangle(boxedImage, (Rect)bb, Scalar(128, 255, 128), 1, LINE_AA);
    }
    imshow("Result", boxedImage
    );
    waitKey();
}

