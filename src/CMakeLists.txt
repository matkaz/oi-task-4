cmake_minimum_required(VERSION 3.5.1)
project(task4)

set(CMAKE_CXX_STANDARD 14)

find_package(OpenCV REQUIRED)

include_directories(${OpenCV_INCLUDE_DIRS})
set(SOURCE_FILES main.cpp BoundingBox.cpp Detector.cpp Detector.h classes.h Trainer.cpp Trainer.h)
add_executable(Task4 ${SOURCE_FILES})
target_link_libraries(Task4 stdc++fs)
target_link_libraries(Task4 ${OpenCV_LIBS})
