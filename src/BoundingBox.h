//
// Created by mateusz on 29.04.17.
//

#ifndef SOLUTIONSEXAMINATOR_BOUNDINGBOX_H
#define SOLUTIONSEXAMINATOR_BOUNDINGBOX_H

#include <string>
#include <sstream>
#include <iostream>
#include <opencv2/core.hpp>

class BoundingBox {
public:
    BoundingBox();
    BoundingBox(double right, double down, double left, double up);
    BoundingBox(const std::istream& input);
    BoundingBox(const cv::Rect &rect);
    BoundingBox(const BoundingBox&) = default;
    BoundingBox(BoundingBox&&) = default;

    BoundingBox(std::istream &input);

    BoundingBox& operator=(const BoundingBox&) = default;
    BoundingBox& operator=(BoundingBox&&) = default;

    /**
     * IOU
     * @param that
     * @return
     */
    double intersectionOverUnion(const BoundingBox& that) const;

    double getArea() const;

    /**
     * Modifies this box so as it covers this box and the given one.
     * @param box
     */
    void enlarge(const BoundingBox& box);

    /**
     * Returns area of intersection of this and that BoundingBox or 0 if no intersection exists.
     * @param that
     * @return
     */
    const double getIntersectionArea(const BoundingBox &that) const;

    BoundingBox operator*(const BoundingBox& that) const;

    operator std::string() const;
    operator cv::Rect_<int>() const;

    friend std::ostream& operator<<(std::ostream& os, const BoundingBox& boundingBox);

    class BoundingBoxException : public std::exception
    {
    public:
        BoundingBoxException(const std::string &message_) : message(message_) {}

        const char *what() const noexcept override {
            return message.c_str();
        }

    private:
        std::string message;
    };

    double getRight() const;

    double getDown() const;

    double getLeft() const;

    double getUp() const;

    double getWidth() const;

    double getHeight() const;

private:
    double right;
    double down;
    double left;
    double up;
};


#endif //SOLUTIONSEXAMINATOR_BOUNDINGBOX_H
