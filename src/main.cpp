#include <iostream>
#include <cstdlib>
#include <memory>
#include "Detector.h"
#include "Trainer.h"

struct Config {
    bool displayMode;
    bool trainMode;
    const char* fileName;
};

void printHelp();
void assertParameters(const int& argc, const char* argv[]);
Config parseConfig(const int &argc, const char **argv);

int main(int argc, const char* argv[]) {
    Config config = parseConfig(argc, argv);

    if (config.trainMode) {
        Trainer trainer(config.fileName);
        trainer.train();
    } else {
        Detector detector(config.fileName);
        detector.testImage();
        detector.printAnnotations(std::cout);

        if (config.displayMode) {
            detector.drawBoxes();
        }
    }

    return EXIT_SUCCESS;
}

void printHelp() {
    std::cerr << "Usage: [-dt] solution1 IMAGE_PATH|TRAINING_DATA_DIR_PATH" << std::endl << std::endl;
    std::cerr << "-t\tTraining mode. Uses TRAINING_DATA_DIR_PATH and saves result into data directory." << std::endl;
    std::cerr << "-d\tDisplay mode. Uses IMAGE_PATH" << std::endl;
}

void assertParameters(const int& argc, const char* argv[]) {
    if (argc < 2 || argc > 3) {
        std::cerr << "Wrong parameters number!" << std::endl;
        printHelp();
        exit(EXIT_FAILURE);
    }
}

Config parseConfig(const int &argc, const char **argv) {
    assertParameters(argc, argv);
    Config config;

    memset(&config, 0, sizeof(config));

    const bool flags = std::string(argv[1])[0] == '-';
    if (flags) {
        config.trainMode = (std::string(argv[1]).find('t', 1) != std::string::npos);
        if (!config.trainMode) config.displayMode = (std::string(argv[1]).find('d', 1) != std::string::npos);
        config.fileName = argv[2];
    } else {
        config.fileName = argv[1];
    }

    return config;
}
