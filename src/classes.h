//
// Created by mateusz on 03.06.17.
//

#ifndef TASK4_CLASSES_H
#define TASK4_CLASSES_H

enum DataClass : unsigned char {
    BACKGROUND = 0,
    ROAD_SIGN = 1
};

#endif //TASK4_CLASSES_H
