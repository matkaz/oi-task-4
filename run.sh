#!/bin/bash

if [ ! -d 'bin' ]; then
    $(realpath ./build.sh)
fi

if [ ! -d 'deploy' ]; then
    ./deploy.sh
fi

IMG_PATH=$1
RESULTS_DIR=$2

for file in $(ls $IMG_PATH)
do
    echo "$RESULTS_DIR/$file"
    if [ ! -e "$RESULTS_DIR/$file.result" ]; then
        ./deploy/Task4 $IMG_PATH/$file > $RESULTS_DIR/$file.result
    fi
done
