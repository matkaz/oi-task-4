#!/bin/bash

# Clone CImage
if [ ! -d 'run' ]; then
    mkdir run
fi
cd run

#OpenCV
if [ ! -d 'opencv-3.2.0' ]; then
    wget https://github.com/opencv/opencv/archive/3.2.0.tar.gz
    mkdir OpenCV
    tar -xzf 3.2.0.tar.gz
    cd opencv-3.2.0
    cmake -D OPENCV_BUILD_3RDPARTY_LIBS=1 -D CMAKE_BUILD_TYPE=Release .
    make -j4
    sudo make install
    # TODO Some more elegant way (However mind that OpenCV is not so elegant at all).
    cd ..
fi
cd ..


# Build solutionMK
if [ ! -d 'bin' ]; then
    mkdir bin
fi
cd bin
cmake ../src
make
rm -rf CMakeFiles
rm cmake_install.cmake
rm CMakeCache.txt
rm Makefile
